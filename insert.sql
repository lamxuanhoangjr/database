-- Insert LopHoc
CALL insert_lophoc(1, 'D1');

-- Insert MonHoc
CALL insert_monhoc(1, 1, 'Lịch sử');

-- Insert Chuong
CALL insert_chuong(1, 1, 1, 'Bối cảnh quốc tế từ sau chiến tranh thế giới thứ hai');
CALL insert_chuong(2, 1, 2, 'Liên Xô và các nước Đông Âu (1945-1991). Liên Bang Nga (1991-2000)');
CALL insert_chuong(3, 1, 3, 'Các nước Á, Phi, và Mĩ La Tinh (1945-2000)');
CALL insert_chuong(4, 1, 4, 'Mĩ, Tây Âu, Nhật Bản (1945-2000)');
CALL insert_chuong(5, 1, 5, 'Quan hệ quốc tế (1945-2000)');

-- Chuong 1
CALL insert_cauhoi(1, 1, 'Hội nghị Ianta (1945) có sự tham gia của các nước nào?');
CALL insert_cautraloi(1, 1, 'Anh - Pháp - Mĩ');
CALL insert_cautraloi(2, 1, 'Anh - Mĩ - Liên Xô');
CALL insert_cautraloi(3, 1, 'Anh - Pháp - Đức');
CALL insert_cautraloi(4, 1, 'Mĩ - Liên Xô - Trung Quốc');
CALL update_cauhoi_cautraloi(1, 2);

CALL insert_cauhoi(2, 1, 'Hội nghị Ianta được triệu tập vào thời điểm nào của cuộc Chiến tranh thế giới thứ hai (1939 – 1945)?');
CALL insert_cautraloi(5, 2, 'Chiến tranh thế giới thứ hai bùng nổ');
CALL insert_cautraloi(6, 2, 'Chiến tranh thế giới thứ hai bước vào giai đoạn ác liệt');
CALL insert_cautraloi(7, 2, 'Chiến tranh thế giới thứ hai bước vào giai đoạn kết thúc');
CALL insert_cautraloi(8, 2, 'Chiến tranh thế giới thứ hai đã kết thúc');
CALL update_cauhoi_cautraloi(2, 7);

CALL insert_cauhoi(3, 1, 'Tương lai của Nhật Bản được quyết định như thế nào theo Hội nghị Ianta (2-1945)?');
CALL insert_cautraloi(9, 3, 'Nhật Bản bị quân đội Mĩ chiếm đóng');
CALL insert_cautraloi(10, 3, 'Nhật Bản vẫn giữ nguyên trạng');
CALL insert_cautraloi(11, 3, 'Quân đội Liên Xô chiếm 4 đảo thuộc quần đảo Curin của Nhật Bản');
CALL insert_cautraloi(12, 3, 'Nhật Bản trở thành thuộc địa kiểu mới của Mĩ');
CALL update_cauhoi_cautraloi(3, 9);

CALL insert_cauhoi(4, 1, 'Theo quyết định của hội nghị Ianta (2-1945), quốc gia nào cần phải trở thành một quốc gia thống nhất và dân chủ?');
CALL insert_cautraloi(13, 4, 'Đức');
CALL insert_cautraloi(14, 4, 'Mông Cổ');
CALL insert_cautraloi(15, 4, 'Trung Quốc');
CALL insert_cautraloi(16, 4, 'Triều Tiên');
CALL update_cauhoi_cautraloi(4, 15);

CALL insert_cauhoi(5, 1, 'Theo quy định của Hội nghị Ianta (2-1945), quốc gia nào sẽ thực hiện nhiệm vụ chiếm đóng, giải giáp miền Tây Đức, Tây Béc-lin và các nước Tây Âu?');
CALL insert_cautraloi(17, 5, 'Liên Xô');
CALL insert_cautraloi(18, 5, 'Mĩ');
CALL insert_cautraloi(19, 5, 'Mĩ, Anh');
CALL insert_cautraloi(20, 5, ' Mĩ, Anh, Pháp');
CALL update_cauhoi_cautraloi(5, 20);

CALL insert_cauhoi(6, 1, 'Theo quy định của Hội nghị Ianta, quân đội nước nào sẽ chiếm đóng các vùng Đông Đức, Đông Âu, Đông Bắc Triều Tiên sau chiến tranh thế giới thứ hai?');
CALL insert_cautraloi(21, 6, 'Liên Xô');
CALL insert_cautraloi(22, 6, 'Mĩ');
CALL insert_cautraloi(23, 6, 'Anh');
CALL insert_cautraloi(24, 6, 'Pháp');
CALL update_cauhoi_cautraloi(6, 21);

CALL insert_cauhoi(7, 1, 'Theo nội dung của Hội nghị Pốtxđam, việc giải giáp quân Nhật ở Đông Dương được giao cho ai?');
CALL insert_cautraloi(25, 7, 'Quân đội Anh trên toàn Việt Nam');
CALL insert_cautraloi(26, 7, 'Quân đội Pháp ở phía Nam vĩ tuyến 16');
CALL insert_cautraloi(27, 7, 'Quân đội Anh ở phía Nam vĩ tuyến 16 và quân đội Trung Hoa Dân quốc vào phía Bắc');
CALL insert_cautraloi(28, 7, 'Quân đội Trung Hoa Dân quốc vào phía Bắc vĩ tuyến 16 và quân đội Pháp ở phía Nam');
CALL update_cauhoi_cautraloi(7, 27);

CALL insert_cauhoi(8, 1, 'Các vùng Đông Nam Á, Nam Á, Tây Á thuộc phạm vi ảnh hưởng của quốc gia nào theo quy định của Hội nghị Ianta (2-1945)?');
CALL insert_cautraloi(29, 8, 'Liên Xô, Mĩ, Anh');
CALL insert_cautraloi(30, 8, 'Các nước phương Tây từng chiếm đóng ở đây');
CALL insert_cautraloi(31, 8, 'Hoa Kỳ, Anh, Pháp');
CALL insert_cautraloi(32, 8, 'Anh, Đức, Nhật Bản');
CALL update_cauhoi_cautraloi(8, 30);

CALL insert_cauhoi(9, 1, 'Nội dung nào không phải là mục đích triệu tập Hội nghị Ianta (tháng 2-1945)?');
CALL insert_cautraloi(33, 9, 'Nhanh chóng đánh bại hoàn toàn các nước phát xít');
CALL insert_cautraloi(34, 9, 'Thành lập khối Đồng minh chống phát xít');
CALL insert_cautraloi(35, 9, 'Tổ chức lại thế giới sau chiến tranh');
CALL insert_cautraloi(36, 9, 'Phân chia thành quả giữa các nước thắng trận');
CALL update_cauhoi_cautraloi(9, 34);

CALL insert_cauhoi(10, 1, 'Đâu không phải là nguyên nhân dẫn tới việc các cường quốc đồng minh triệu tập Hội nghị Ianta (2-1945)?');
CALL insert_cautraloi(37, 10, 'Yêu cầu nhanh chóng đánh bại hoàn toàn các nước phát xít');
CALL insert_cautraloi(38, 10, 'Yêu cầu tổ chức lại thế giới sau chiến tranh');
CALL insert_cautraloi(39, 10, 'Yêu cầu thắt chặt khối đồng minh chống phát xít');
CALL insert_cautraloi(40, 10, 'Phân chia thành quả chiến thắng giữa các nước thắng trận');
CALL update_cauhoi_cautraloi(10, 39);

-- Chuong 2
CALL insert_cauhoi(11, 2, 'Kế hoạch 5 năm (1946-1950) của nhân dân Xô Viết thực hiện trong hoàn cảnh nào?');
CALL insert_cautraloi(41, 11, 'Là nước thắng trận, Liên Xô thu được nhiều thành quả từ trong Chiến tranh thế giới thứ hai.');
CALL insert_cautraloi(42, 11, 'Chiến tranh thế giới thứ hai để lại hậu quả nặng nề.');
CALL insert_cautraloi(43, 11, 'Khôi phục kinh tế, hằn gắn vết thương chiến tranh.');
CALL insert_cautraloi(44, 11, 'Liên Xô cần xây dựng cơ sở vật chất kĩ thuật cho chủ nghĩa xã hội.');
CALL update_cauhoi_cautraloi(11, 42);

CALL insert_cauhoi(12, 2, 'Kế hoạch 5 năm (1946-1950) nhân dân Xô Viết thực hiện nhằm mục đích');
CALL insert_cautraloi(45, 12, 'khôi phục kinh tế, hàn gắt vết thương chiến tranh');
CALL insert_cautraloi(46, 12, 'củng cố quốc phòng an ninh');
CALL insert_cautraloi(47, 12, 'xây dựng cơ sở vật chất kĩ thuật cho chủ nghĩa xã hội');
CALL insert_cautraloi(48, 12, 'công nghiệp hóa xã hội chủ nghĩa');
CALL update_cauhoi_cautraloi(12, 45);

CALL insert_cauhoi(13, 2, 'Kế hoạch 5 năm (1946-1950) của Liên Xô được tiến hành trong thời gian bao lâu?');
CALL insert_cautraloi(49, 13, '4 năm 3 tháng');
CALL insert_cautraloi(50, 13, '1 năm 3 tháng');
CALL insert_cautraloi(51, 13, '12 tháng');
CALL insert_cautraloi(52, 13, '9 tháng');
CALL update_cauhoi_cautraloi(13, 49);

CALL insert_cauhoi(14, 2, 'Kế hoạch 5 năm (1946-1950) được Liên Xô tiến hành đã hoàn thành trước thời hạn bao lâu?');
CALL insert_cautraloi(53, 14, '1 năm 3 tháng');
CALL insert_cautraloi(54, 14, '9 tháng');
CALL insert_cautraloi(55, 14, '12 tháng');
CALL insert_cautraloi(56, 14, '10 tháng');
CALL update_cauhoi_cautraloi(14, 54);

CALL insert_cauhoi(15, 2, 'Sự kiện Liên Xô chế tạo thành công bom nguyên tử (1949) đã');
CALL insert_cautraloi(57, 15, 'Phá vỡ thế độc quyền nguyên tử của Mỹ');
CALL insert_cautraloi(58, 15, 'Làm đảo lộn chiến lược toàn cầu của Mỹ');
CALL insert_cautraloi(59, 15, 'Buộc các nước phương Tây phải nể sợ');
CALL insert_cautraloi(60, 15, 'Khởi đầu sự đối đầu giữa Liên Xô và Mỹ');
CALL update_cauhoi_cautraloi(15, 57);

CALL insert_cauhoi(16, 2, 'Năm 1949, Khoa học - kĩ thuật Liên Xô có bước phát triển nhanh chóng được đánh dấu bằng sự kiện nào?');
CALL insert_cautraloi(61, 16, 'Liên Xô phóng thành cng vệ tinh nhân tạo');
CALL insert_cautraloi(62, 16, 'Liên Xô đưa người bay vào vũ trụ');
CALL insert_cautraloi(63, 16, 'Liên Xô chế tạo thành công bom nguyên tử');
CALL insert_cautraloi(64, 16, 'Liên Xô phóng thành công tàu phương Đông');
CALL update_cauhoi_cautraloi(16, 63);

CALL insert_cauhoi(17, 2, 'Liên Xô trở thành cường quốc công nghiệp thứ hai thế giới trong khoảng thời gian nào?');
CALL insert_cautraloi(65, 17, 'Từ nửa đầu những năm 70 của thế kỉ XX');
CALL insert_cautraloi(66, 17, 'Đến nửa đầu những năm 70 của thế kỉ XX');
CALL insert_cautraloi(67, 17, 'Từ cuối những năm 70 của thế kỉ XX');
CALL insert_cautraloi(68, 17, 'Đến cuối những năm 70 của thế kỉ XX');
CALL update_cauhoi_cautraloi(17, 66);

CALL insert_cauhoi(18, 2, 'Đến nửa đầu những năm 70, Liên Xô đứng ở vị trí nào trong nền kinh tế thế giới?');
CALL insert_cautraloi(69, 18, 'Siêu cường kinh tế duy nhất thế giới');
CALL insert_cautraloi(70, 18, 'Đến nửa đầu những năm 70 của thế kỉ XX');
CALL insert_cautraloi(71, 18, 'Từ cuối những năm 70 của thế kỉ XX');
CALL insert_cautraloi(72, 18, 'Đến cuối những năm 70 của thế kỉ XX');
CALL update_cauhoi_cautraloi(18, 70);

CALL insert_cauhoi(19, 2, 'Sự kiện nào đã mở đầu kỷ nguyên chinh phục vũ trụ của loài người?');
CALL insert_cautraloi(73, 19, 'Liên Xô chế tạo thành công bom nguyên tử');
CALL insert_cautraloi(74, 19, 'Liên Xô phóng tàu vũ trụ đưa nhà du hành vũ trụ Gagarin bay vòng quanh Trái Đất');
CALL insert_cautraloi(75, 19, 'Neil Armstrong đặt chân lên Mặt Trăng');
CALL insert_cautraloi(76, 19, 'Liên Xô phóng thành công trạm tự động “Mặt Trăng 3” (Luna 3) bay vòng quanh phía sau Mặt Trăng');
CALL update_cauhoi_cautraloi(19, 74);

CALL insert_cauhoi(20, 2, 'Từ năm 1950 đến nửa đầu những năm 70, Liên Xô đi đầu thế giới trong lĩnh vực nào?');
CALL insert_cautraloi(77, 20, 'Công nghiệp nặng, công nghiệp chế tạo');
CALL insert_cautraloi(78, 20, 'Công nghiệp sản xuất hàng tiêu dùng');
CALL insert_cautraloi(79, 20, 'Công nghiệp quốc phòng');
CALL insert_cautraloi(80, 20, 'Công nghiệp vũ trụ, công nghiệp điện hạt nhân');
CALL update_cauhoi_cautraloi(20, 80);

-- Chuong 3
CALL insert_cauhoi(21, 3, 'Khu vực Đông Bắc Á bao gồm các quốc gia nào?');
CALL insert_cautraloi(81, 21, 'Trung Quốc, Nhật Bản, Hàn Quốc, Triều Tiên');
CALL insert_cautraloi(82, 21, 'Trung Quốc, Đài Loan, Hàn Quốc, Triều Tiên');
CALL insert_cautraloi(83, 21, 'Trung Quốc, Nhật Bản, Hàn Quốc, Triều Tiên, Đài Loan');
CALL insert_cautraloi(84, 21, 'Trung Quốc, Nhật Bản, Hàn Quốc, Triều Tiên, Đài Loan, vùng Viễn Đông Liên Bang Nga');
CALL update_cauhoi_cautraloi(21, 81);

CALL insert_cauhoi(22, 3, 'Trước Chiến tranh thế giới thứ hai, tình hình các nước Đông Bắc Á như thế nào?');
CALL insert_cautraloi(85, 22, 'Đều bị các nước thực dân xâm lược.');
CALL insert_cautraloi(86, 22, 'Đều là những quốc gia độc lập.');
CALL insert_cautraloi(87, 22, 'Hầu hết đều bị chủ nghĩa thực dân nô dịch.');
CALL insert_cautraloi(88, 22, 'Có nền kinh tế phát triển.');
CALL update_cauhoi_cautraloi(22, 83);

CALL insert_cauhoi(23, 3, 'Những sự kiện thể hiện sự biến đổi lớn về chính trị của khu vực Đông Bắc Á sau chiến tranh thế giới thứ hai là');
CALL insert_cautraloi(89, 23, 'Trung Quốc thu hồi được Hồng Công');
CALL insert_cautraloi(90, 23, 'Nhật Bản chủ trương liên minh chặt chẽ với Mĩ');
CALL insert_cautraloi(91, 23, 'Sự ra đời của nước CHND Trung Hoa và sự thành lập hai nhà nước trên bán đảo Triều Tiên');
CALL insert_cautraloi(92, 23, 'Mĩ phát động chiến tranh xâm lược Triều Tiên');
CALL update_cauhoi_cautraloi(23, 91);

CALL insert_cauhoi(24, 3, 'Các quốc gia và vùng lãnh thổ nào ở khu vực Đông Bắc Á được mệnh danh là “con rồng” kinh tế châu');
CALL insert_cautraloi(93, 24, 'Hàn Quốc, Nhật Bản, Hồng Công');
CALL insert_cautraloi(94, 24, 'Nhật Bản, Hồng Công, Đài Loan');
CALL insert_cautraloi(95, 24, 'Hàn Quốc, Đài Loan, Hồng Công');
CALL insert_cautraloi(96, 24, 'Hàn Quốc, Nhật Bản, Đài Loan');
CALL update_cauhoi_cautraloi(24, 95);

CALL insert_cauhoi(25, 3, 'Quốc gia và vùng lãnh thổ nào dưới đây không được mệnh danh là “con rồng” kinh tế của châu Á?');
CALL insert_cautraloi(97, 25, 'Hàn Quốc');
CALL insert_cautraloi(98, 25, 'Đài Loan');
CALL insert_cautraloi(99, 25, 'Hồng Công');
CALL insert_cautraloi(100, 25, 'Nhật Bản');
CALL update_cauhoi_cautraloi(25, 100);

CALL insert_cauhoi(26, 3, 'Bán đảo Triều Tiên bị chia cắt thành 2 miền theo vĩ tuyến số bao nhiêu?');
CALL insert_cautraloi(101, 26, 'Vĩ tuyến 39');
CALL insert_cautraloi(102, 26, 'Vĩ tuyến 38');
CALL insert_cautraloi(103, 26, 'Vĩ tuyến 16');
CALL insert_cautraloi(104, 26, 'Vĩ tuyến 37');
CALL update_cauhoi_cautraloi(26, 102);

CALL insert_cauhoi(27, 3, 'Nhà nước Đại Hàn Dân quốc (Hàn Quốc) được thành lập vào thời gian nào và ở đâu?');
CALL insert_cautraloi(105, 27, 'Tháng 8 - 1948, ở phía Nam bán đảo Triều Tiên');
CALL insert_cautraloi(106, 27, 'Tháng 9 - 1948, ở phía Bắc bán đảo Triều Tiên');
CALL insert_cautraloi(107, 27, 'Tháng 8 - 1949, ở phía Nam bán đảo Triều Tiên');
CALL insert_cautraloi(108, 27, 'Tháng 9 - 1949, ở phía Bắc bán đảo Triều Tiên');
CALL update_cauhoi_cautraloi(27, 105);

CALL insert_cauhoi(28, 3, 'Trong những năm 1950-1953, hai miền bán đảo Triều Tiên ở trong tình thế');
CALL insert_cautraloi(109, 28, 'Hòa dịu, hợp tác');
CALL insert_cautraloi(110, 28, 'Tháng 9 - 1948, ở phía Bắc bán đảo Triều Tiên');
CALL insert_cautraloi(111, 28, 'Tháng 8 - 1949, ở phía Nam bán đảo Triều Tiên');
CALL insert_cautraloi(112, 28, 'Tháng 9 - 1949, ở phía Bắc bán đảo Triều Tiên');
CALL update_cauhoi_cautraloi(28, 112);

CALL insert_cauhoi(29, 3, 'Hiệp định hòa hợp giữa hai miền Nam - Bắc Triều Tiên được kí kết từ năm 2000 có ý nghĩa gì?');
CALL insert_cautraloi(113, 29, 'Mở ra thời kì hợp tác cùng phát triển giữa hai miền Nam - Bắc Triều Tiên');
CALL insert_cautraloi(114, 29, 'Mở ra bước mới trong tiến trình hòa hợp, thống nhất bán đảo Triều Tiên');
CALL insert_cautraloi(115, 29, 'Chấm dứt thời kì đối đầu căng thẳng giữa hai miền');
CALL insert_cautraloi(116, 29, 'Chấm dứt tình trạng chia cắt, thống nhất bán đảo Triều Tiên');
CALL update_cauhoi_cautraloi(29, 114);

CALL insert_cauhoi(30, 3, 'Đâu không phải lý do tại sao cho đến nay Đài Loan vẫn nằm ngoài sự kiểm soát của Cộng hòa Nhân dân Trung Hoa?');
CALL insert_cautraloi(117, 30, 'Do Quốc dân Đảng vẫn nắm quyền kiểm soát khu vực này sau cuộc nội chiến 1946 - 1949');
CALL insert_cautraloi(118, 30, 'Mở ra bước mới trong tiến trình hòa hợp, thống nhất bán đảo Triều Tiên');
CALL insert_cautraloi(119, 30, 'Do sự chia rẽ của các thế lực thù địch');
CALL insert_cautraloi(120, 30, 'Do đường lối “một đất nước hai chế độ” nhà nước CHND Trung Hoa muốn thực hiện');
CALL update_cauhoi_cautraloi(30, 119);

-- Chuong 4
CALL insert_cauhoi(31, 4, 'Hậu quả của Chiến tranh thế giới thứ hai (1939-1945) để lại đã làm cho nền kinh tế Tây Âu trở nên');
CALL insert_cautraloi(121, 31, 'Kiệt quệ');
CALL insert_cautraloi(122, 31, 'Phát triển mạnh mẽ');
CALL insert_cautraloi(123, 31, 'Phát triển không ổn định');
CALL insert_cautraloi(124, 31, 'Phát triển chậm');
CALL update_cauhoi_cautraloi(31, 121);

CALL insert_cauhoi(32, 4, 'Nguyên nhân cơ bản giúp kinh tế Tây Âu phát triển sau chiến tranh thế giới thứ 2 là');
CALL insert_cautraloi(125, 32, 'Nguồn viện trợ của Mỹ thông qua kế hoạch Macsan');
CALL insert_cautraloi(126, 32, 'Tài nguyên thiên nhiên giàu có, nhân lực lao động dồi dào');
CALL insert_cautraloi(127, 32, 'Tận dụng tốt cơ hội bên ngoài và áp dụng thành công khoa học kỹ thuật');
CALL insert_cautraloi(128, 32, 'Quá trình tập trung tư bản và tập trung lao động cao');
CALL update_cauhoi_cautraloi(32, 123);

CALL insert_cauhoi(33, 4, 'Năm 1947, Mĩ đề ra và thực hiện “kế hoạch Mácsan” nhằm mục đích chính trị gì?');
CALL insert_cautraloi(129, 33, 'Tạo ra căn cứ tiền phương chống Liên Xô');
CALL insert_cautraloi(130, 33, 'Tạo ra sự đối trọng với khối Đông Âu xã hội chủ nghĩa');
CALL insert_cautraloi(131, 33, 'Tìm kiếm đồng minh chống lại Liên Xô và Đông Âu');
CALL insert_cautraloi(132, 33, 'Củng cố ảnh hưởng của Mĩ ở châu Âu');
CALL update_cauhoi_cautraloi(33, 131);

CALL insert_cauhoi(34, 4, 'Từ năm 1945 đến 1950, các nước tư bản Tây Âu dựa vào đâu để đạt được sự phục hồi cơ bản về mọi mặt?');
CALL insert_cautraloi(133, 34, 'Hợp tác thành công với Nhật');
CALL insert_cautraloi(134, 34, 'Mở rộng quan hệ với Liên Xô');
CALL insert_cautraloi(135, 34, 'Viện trợ của Mĩ qua kế hoạch Macsan');
CALL insert_cautraloi(136, 34, 'Đẩy mạnh xuất khẩu hàng hóa đến các nước thứ 3');
CALL update_cauhoi_cautraloi(34, 134);

CALL insert_cauhoi(35, 4, 'Đến đầu thập kỉ 70 của thế kỉ XX, Tây Âu đã đạt được thành tựu gì quan trọng về kinh tế?');
CALL insert_cautraloi(137, 35, 'Trở thành khối kinh tế đứng thứ hai thế giới');
CALL insert_cautraloi(138, 35, 'Trở thành một trong ba trung tâm kinh tế- tài chính của thế giới');
CALL insert_cautraloi(139, 35, 'Trở thành trung tâm kinh tế đứng đầu khối tư bản chủ nghĩa');
CALL insert_cautraloi(140, 35, 'Trở thành trung tâm công nghiệp - quốc phòng lớn nhất thế giới');
CALL update_cauhoi_cautraloi(35, 138);

CALL insert_cauhoi(36, 4, 'Từ năm 1973 - 1991, kinh tế của các nước tư bản Tây Âu');
CALL insert_cautraloi(141, 36, 'Lâm vào khủng hoảng, suy thoái, phát triển không ổn định');
CALL insert_cautraloi(142, 36, 'Phát triển ổn định và đạt mức tăng trưởng cao');
CALL insert_cautraloi(143, 36, 'Phát triển không đồng đều do sự sụp đổ của hệ thống thuộc địa');
CALL insert_cautraloi(144, 36, 'Vươn lên hàng thứ hai thế giới');
CALL update_cauhoi_cautraloi(36, 141);

CALL insert_cauhoi(37, 4, 'Điểm nhất quán trong chính sách đối ngoại của các nước Tây Âu giai đoạn 1945-1950 là');
CALL insert_cautraloi(145, 37, 'Mở rộng hợp tác với Nhật Bản và Hàn Quốc');
CALL insert_cautraloi(146, 37, 'Liên kết chống lại các nước Đông Âu');
CALL insert_cautraloi(147, 37, 'Liên minh với CHLB Đức');
CALL insert_cautraloi(148, 37, 'Liên minh chặt chẽ với Mĩ');
CALL update_cauhoi_cautraloi(37, 148);

CALL insert_cauhoi(38, 4, 'Trong giai đoạn 1991 - 2000 ở Tây Âu, những nước nào đã trở thành đối trọng với Mỹ trong nhiều vấn đề quốc tế quan trọng?');
CALL insert_cautraloi(149, 38, 'Anh, Pháp');
CALL insert_cautraloi(150, 38, 'Pháp, Đức');
CALL insert_cautraloi(151, 38, 'Anh, Hà Lan');
CALL insert_cautraloi(152, 38, 'Đức, Anh');
CALL update_cauhoi_cautraloi(38, 150);

CALL insert_cauhoi(39, 4, 'Sau Chiến tranh thế giới thứ hai (1939-1945), các nước Tây Âu có hành động gì đối với các thuộc địa thuộc địa cũ?');
CALL insert_cautraloi(153, 39, 'Đa số ủng hộ vấn đề độc lập ở các thuộc địa');
CALL insert_cautraloi(154, 39, 'Tìm cách biến các nước thuộc thế giới thứ ba thành thuộc địa kiểu mới');
CALL insert_cautraloi(155, 39, 'Ủng hộ việc thiết lập quyền tự trị ở các thuộc địa');
CALL insert_cautraloi(156, 39, 'Tìm cách tái thiết lập chủ quyền ở các thuộc địa cũ');
CALL update_cauhoi_cautraloi(39, 150);

CALL insert_cauhoi(40, 4, 'Chính sách đối ngoại chủ yếu của Tây Âu từ 1950 đến 1973 là gì?');
CALL insert_cautraloi(157, 40, 'Cố gắng quan hệ với Nhật Bản');
CALL insert_cautraloi(158, 40, 'Đa phương hóa trong quan hệ');
CALL insert_cautraloi(159, 40, 'Liên minh hoàn toàn với Mỹ');
CALL insert_cautraloi(160, 40, 'Rút ra khỏi NATO');
CALL update_cauhoi_cautraloi(40, 158);

-- Chuong 5
CALL insert_cauhoi(41, 5, 'Sau chiến tranh thế giới thứ hai, quan hệ giữa Mĩ và Liên Xô đã có sự chuyển biến như thế nào?');
CALL insert_cautraloi(161, 41, 'Chuyển từ đối đầu sang đối thoại, thực hiện hợp tác trên nhiều lĩnh vực');
CALL insert_cautraloi(162, 41, 'Hợp tác với nhau trong việc giải quyết nhiều vấn đề quốc tế lớn');
CALL insert_cautraloi(163, 41, 'Từ hợp tác sang đối đầu trực tiếp với các cuộc chiến tranh cục bộ lớn diễn ra');
CALL insert_cautraloi(164, 41, 'Từ đồng minh trong chiến tranh chuyển sang đối đầu và đi đến tình trạng chiến tranh lạnh');
CALL update_cauhoi_cautraloi(41, 164);

CALL insert_cauhoi(42, 5, 'Nguyên nhân dẫn đến sự đối đầu giữa hai cường quốc Liên Xô và Mĩ là');
CALL insert_cautraloi(165, 42, 'Sự đối lập về mục tiêu và chiến lược');
CALL insert_cautraloi(166, 42, 'Sự đối lập về chế độ chính trị');
CALL insert_cautraloi(167, 42, 'Sự đối lập về khuynh hướng phát triển');
CALL insert_cautraloi(168, 42, 'Sự đối lập về chính sách đối nội, đối ngoại');
CALL update_cauhoi_cautraloi(42, 123);

CALL insert_cauhoi(43, 5, ' Sự kiện nào được xem là khởi đầu cho chính sách chống Liên Xô, gây nên cuộc Chiến tranh lạnh?');
CALL insert_cautraloi(169, 43, 'Thông điệp của Tổng thống Truman tại Quốc hội Mĩ (1947)');
CALL insert_cautraloi(170, 43, 'Kế hoạch Mácsan (1947)');
CALL insert_cautraloi(171, 43, 'Sự ra đời của tổ chức Hiệp ước Bắc Đại Tây Dương (NATO) (1949)');
CALL insert_cautraloi(172, 43, 'Sự ra đời của tổ chức Hiệp ước VACSAVA (1955)');
CALL update_cauhoi_cautraloi(43, 169);

CALL insert_cauhoi(44, 5, 'Sự kiện nào xác lập Chiến tranh lạnh bao trùm cả thế giới sau chiến tranh thế giới thứ hai?');
CALL insert_cautraloi(173, 44, 'Học thuyết của tổng thống Truman');
CALL insert_cautraloi(174, 44, 'Học thuyết của Tổng thống Ri-gân');
CALL insert_cautraloi(175, 44, 'Sự ra đời của NATO và Vacsava');
CALL insert_cautraloi(176, 44, 'Chiến lược cam kết và mở rộng');
CALL update_cauhoi_cautraloi(44, 175);

CALL insert_cauhoi(45, 5, 'Đến đầu thập kỉ 70 của thế kỉ XX, Tây Âu đã đạt được thành tựu gì quan trọng về kinh tế?');
CALL insert_cautraloi(177, 45, 'Trở thành khối kinh tế đứng thứ hai thế giới');
CALL insert_cautraloi(178, 45, 'Trở thành một trong ba trung tâm kinh tế- tài chính của thế giới');
CALL insert_cautraloi(179, 45, 'Trở thành trung tâm kinh tế đứng đầu khối tư bản chủ nghĩa');
CALL insert_cautraloi(180, 45, 'Trở thành trung tâm công nghiệp - quốc phòng lớn nhất thế giới');
CALL update_cauhoi_cautraloi(45, 178);

CALL insert_cauhoi(46, 5, 'Sự ra đời của tổ chức hiệp ước Bắc Đại Tây Dương (1949) và tổ chức hiệp ước Vacsava (1955) đã tác động như thế nào đến quan hệ quốc tế?');
CALL insert_cautraloi(181, 46, 'Đặt nhân loại đứng trước nguy cơ của cuộc chiến tranh thế giới mới');
CALL insert_cautraloi(182, 46, 'Xác lập cục diện hai phe, hai cực, chiến tranh lạnh bao trùm thế giới');
CALL insert_cautraloi(183, 46, 'Đánh dấu cuộc chiến tranh lạnh chính thức bắt đầu');
CALL insert_cautraloi(184, 46, 'Tạo nên sự phân chia đối lập giữa Đông Âu và Tây Âu');
CALL update_cauhoi_cautraloi(46, 182);

CALL insert_cauhoi(47, 5, 'Từ đầu những năm 70 của thế kỉ XX, tình hình quan hệ quốc tế đã có chuyển biến gì?');
CALL insert_cautraloi(185, 47, 'Chuyển từ đối đầu sang đối thoại');
CALL insert_cautraloi(186, 47, 'Tiếp tục đối đầu căng thẳng');
CALL insert_cautraloi(187, 47, 'Xu hướng hòa hoãn xuất hiện');
CALL insert_cautraloi(188, 47, 'Thiết lập quan hệ đồng minh');
CALL update_cauhoi_cautraloi(47, 188);

CALL insert_cauhoi(48, 5, 'Định ước Henxiki (năm 1975) được ký kết giữa');
CALL insert_cautraloi(189, 48, 'Mỹ - Anh - Pháp - Cộng hòa Dân chủ Đức và Liên Xô');
CALL insert_cautraloi(190, 48, '33 nước châu Âu cùng với Mỹ và Canada');
CALL insert_cautraloi(191, 48, 'Các nước châu Âu');
CALL insert_cautraloi(192, 48, 'Cộng hòa Dân chủ Đức, Mỹ, Canada');
CALL update_cauhoi_cautraloi(48, 190);

CALL insert_cauhoi(49, 5, 'Tháng 12-1989 đã diễn ra sự kiện lịch sử gì trong quan hệ quốc tế?');
CALL insert_cautraloi(193, 49, 'Trật tự hai cực Ianta sụp đổ');
CALL insert_cautraloi(194, 49, 'Nước Đức được thống nhất');
CALL insert_cautraloi(195, 49, 'Bức tường Béc lin sụp đổ');
CALL insert_cautraloi(196, 49, 'Chiến tranh lạnh chấm dứt');
CALL update_cauhoi_cautraloi(49, 196);

CALL insert_cauhoi(50, 5, 'Tháng 11-1972 đã diễn ra sự kiện lịch sử gì trong quan hệ quốc tế?');
CALL insert_cautraloi(197, 50, 'Kí kết hiệp định về những cơ sở của quan hệ giữa Đông Đức và Tây Đức');
CALL insert_cautraloi(198, 50, 'Kí kết hiệp ước về việc hạn chế hệ thống phòng chống tên lửa (ABM)');
CALL insert_cautraloi(199, 50, 'Kí kết Định ước Henxinki');
CALL insert_cautraloi(200, 50, 'Cuộc gặp gỡ cấp cao giữa hai nhà lãnh đạo Mĩ và Liên Xô');
CALL update_cauhoi_cautraloi(50, 197);