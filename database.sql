CREATE TABLE LopHoc(
    lophoc_id    INT PRIMARY KEY,
    lophoc_name  VARCHAR(100)
);

CREATE TABLE MonHoc(
    monhoc_id   INT PRIMARY KEY,
    lophoc_id   INT,
    monhoc_name VARCHAR(100) DEFAULT '',
    CONSTRAINT fk_lophoc_cho_monhoc
        FOREIGN KEY (lophoc_id)
        REFERENCES LopHoc(lophoc_id)
);

CREATE TABLE Chuong(
    chuong_id     INT PRIMARY KEY,
    monhoc_id     INT,
    chuong_number INT DEFAULT 1,
    chuong_name   VARCHAR(200) DEFAULT '',
    CONSTRAINT fk_monhoc_cho_chuong
        FOREIGN KEY (monhoc_id)
        REFERENCES MonHoc(monhoc_id)
);

CREATE TABLE CauHoi(
    cauhoi_id    INT PRIMARY KEY,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  VARCHAR(800) DEFAULT '',
    CONSTRAINT fk_chuong_cho_cauhoi
        FOREIGN KEY (chuong_id)
        REFERENCES Chuong(chuong_id)
);

CREATE TABLE CauTraLoi(
    cautraloi_id   INT PRIMARY KEY,
    cauhoi_id      INT,
    cautraloi_name VARCHAR(800) DEFAULT '',
    CONSTRAINT fk_cauhoi_cho_cautraloi
        FOREIGN KEY (cauhoi_id)
        REFERENCES CauHoi(cauhoi_id)
);

ALTER TABLE CauHoi ADD
    CONSTRAINT fk_cautraloi_cho_cauhoi
    FOREIGN KEY (cautraloi_id)
    REFERENCES CauTraLoi(cautraloi_id);

CREATE TABLE DeThi(
    dethi_id   INT PRIMARY KEY,
    monhoc_id  INT,
    dethi_time INT,
    dethi_name VARCHAR(100) DEFAULT '',
    CONSTRAINT fk_monhoc_cho_dethi
        FOREIGN KEY (monhoc_id)
        REFERENCES MonHoc(monhoc_id)
);

CREATE TABLE CauHoiDeThi(
    cauhoidethi_id   INT PRIMARY KEY,
    dethi_id         INT,
    cauhoi_id        INT,
    CONSTRAINT fk_dethi_cho_cauhoidethi
        FOREIGN KEY (dethi_id)
        REFERENCES DeThi(dethi_id),
    CONSTRAINT fk_cauhoi_cho_cauhoidethi
        FOREIGN KEY (cauhoi_id)
        REFERENCES CauHoi(cauhoi_id)
);

CREATE TABLE Quyen(
    quyen_id   INT PRIMARY KEY,
    quyen_name VARCHAR(100)
);

CREATE TABLE NguoiDung(
    nguoidung_account     VARCHAR(255) PRIMARY KEY,
    quyen_id              INT,
    nguoidung_password    VARCHAR(255),
    nguoidung_displayname VARCHAR(100) DEFAULT '',
    CONSTRAINT fk_quyen_cho_nguoidung
        FOREIGN KEY (quyen_id)
        REFERENCES Quyen(quyen_id)
);

CREATE TABLE SinhVien(
    sinhvien_id       INT PRIMARY KEY,
    nguoidung_account VARCHAR(255),
    sinhvien_name     VARCHAR(100),
    sinhvien_gender   VARCHAR(3) CHECK (sinhvien_gender='Nam' OR sinhvien_gender='Nữ'),
    sinhvien_birthday TIMESTAMP WITHOUT TIME ZONE DEFAULT '2000-01-01 00:00:00',
    CONSTRAINT fk_nguoidung_cho_sinhvien
        FOREIGN KEY (nguoidung_account)
        REFERENCES NguoiDung(nguoidung_account)
);

CREATE TABLE BaiThi(
    baithi_id         INT PRIMARY KEY,
    dethi_id          INT,
    sinhvien_id       INT,
    baithi_startdate  TIMESTAMP WITHOUT TIME ZONE DEFAULT '2000-01-01 00:00:00',
    baithi_enddate    TIMESTAMP WITHOUT TIME ZONE DEFAULT '2000-01-01 00:00:00',
    CONSTRAINT fk_dethi_cho_baithi
        FOREIGN KEY (dethi_id)
        REFERENCES DeThi(dethi_id),
    CONSTRAINT fk_sinhvien_cho_baithi
        FOREIGN KEY (sinhvien_id)
        REFERENCES SinhVien(sinhvien_id)
);

CREATE TABLE CauTraLoiBaiThi(
    cautraloibaithi_id INT PRIMARY KEY,
    baithi_id          INT,
    cautraloi_id       INT,
    CONSTRAINT fk_baithi_cho_cautraloibaithi
        FOREIGN KEY (baithi_id)
        REFERENCES BaiThi(baithi_id),
    CONSTRAINT fk_cautraloi_cho_cautraloibaithi
        FOREIGN KEY (cautraloi_id)
        REFERENCES CauTraLoi(cautraloi_id)
);
