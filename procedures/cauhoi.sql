/* Insert */
CREATE OR REPLACE PROCEDURE insert_cauhoi(
    p_id        INT,
    p_chuong_id INT,
    p_name      VARCHAR(800)
) AS
$$
BEGIN
    INSERT INTO CauHoi (cauhoi_id, chuong_id, cauhoi_name)
    VALUES (p_id, p_chuong_id, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cauhoi(
    p_id           INT,
    p_chuong_id    INT,
    p_cautraloi_id INT,
    p_name         VARCHAR(800)
) AS
$$
BEGIN
    UPDATE CauHoi
    SET
        chuong_id = p_chuong_id,
        cautraloi_id = p_cautraloi_id,
        cauhoi_name = p_name
    WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cauhoi_cautraloi(
    p_id           INT,
    p_cautraloi_id INT
) AS
$$
BEGIN
    UPDATE CauHoi
    SET cautraloi_id = p_cautraloi_id
    WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_cauhoi(p_id INT)
AS
$$
BEGIN
    DELETE FROM CauHoi
    WHERE cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;
