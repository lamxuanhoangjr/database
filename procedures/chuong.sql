/* Insert */
CREATE OR REPLACE PROCEDURE insert_chuong(
    p_id         INT,
    p_monhoc_id  INT,
    p_number     INT,
    p_name       VARCHAR(200)
) AS
$$
BEGIN
    INSERT INTO Chuong (chuong_id, monhoc_id, chuong_number, chuong_name)
    VALUES (p_id, p_monhoc_id, p_number, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_chuong(
    p_id         INT,
    p_monhoc_id  INT,
    p_number     INT,
    p_name       VARCHAR(200)
) AS
$$
BEGIN
    UPDATE MonHoc
    SET
        monhoc_id = p_monhoc_id,
        chuong_number = p_number,
        chuong_name = p_name
    WHERE chuong_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_chuong(p_id INT)
AS
$$
BEGIN
    DELETE FROM Chuong
    WHERE chuong_id = p_id;
END;
$$
LANGUAGE plpgsql;