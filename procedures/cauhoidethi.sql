/* Insert */
CREATE OR REPLACE PROCEDURE insert_cauhoidethi(
    p_id         INT,
    p_dethi_id   INT,
    p_cauhoi_id  INT
) AS
$$
BEGIN
    INSERT INTO CauHoiDeThi (cauhoidethi_id, dethi_id, cauhoi_id)
    VALUES (p_id, p_dethi_id, p_cauhoi_id);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cauhoidethi(
    p_id         INT,
    p_dethi_id   INT,
    p_cauhoi_id  INT
) AS
$$
BEGIN
    UPDATE CauHoiDeThi
    SET
        dethi_id = p_dethi_id,
        cauhoi_id = p_cauhoi_id
    WHERE cauhoidethi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_cauhoidethi(p_id INT)
AS
$$
BEGIN
    DELETE FROM CauHoiDeThi
    WHERE cauhoidethi_id = p_id;
END;
$$
LANGUAGE plpgsql;