/* Insert */
CREATE OR REPLACE PROCEDURE insert_caitraloibaithi(
    p_id           INT,
    p_baithi_id     INT,
    p_cautraloi_id  INT
) AS
$$
BEGIN
    INSERT INTO BaiThi (
        cautraloibaithi_id,
        baithi_id,
        cautraloi_id
    )
    VALUES (p_id, p_baithi_id, p_cautraloi_id);
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_caitraloibaithi(p_id INT)
AS
$$
BEGIN
    DELETE FROM CauTraLoiBaiThi
    WHERE cautraloibaithi_id = p_id;
END;
$$
LANGUAGE plpgsql;
