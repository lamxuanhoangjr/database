/* Insert */
CREATE OR REPLACE PROCEDURE insert_nguoidung(
    p_account     VARCHAR(255),
    p_quyen_id    INT,
    p_password    VARCHAR(255),
    p_displayname VARCHAR(100)
) AS
$$
BEGIN
    INSERT INTO NguoiDung (nguoidung_account, quyen_id, nguoidung_password, nguoidung_displayname)
    VALUES (p_account, p_quyen_id, p_password, p_displayname);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_nguoidung(
    p_account     VARCHAR(255),
    p_quyen_id    INT,
    p_password    VARCHAR(255),
    p_displayname VARCHAR(100)
) AS
$$
BEGIN
    UPDATE NguoiDung
    SET
        quyen_id = p_quyen_id,
        nguoidung_password = p_password,
        nguoidung_displayname = p_displayname
    WHERE nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_nguoidung(p_account VARCHAR(255))
AS
$$
BEGIN
    DELETE FROM NguoiDung
    WHERE nguoidung_account = p_account;
END;
$$
LANGUAGE plpgsql;