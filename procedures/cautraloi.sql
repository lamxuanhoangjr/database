/* Insert */
CREATE OR REPLACE PROCEDURE insert_cautraloi(
    p_id         INT,
    p_cauhoi_id  INT,
    p_name       VARCHAR(800)
) AS
$$
BEGIN
    INSERT INTO CauTraLoi (cautraloi_id, cauhoi_id, cautraloi_name)
    VALUES (p_id, p_cauhoi_id, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_cautraloi(
    p_id         INT,
    p_cauhoi_id  INT,
    p_name       VARCHAR(800)
) AS
$$
BEGIN
    UPDATE CauTraLoi
    SET
        cauhoi_id = p_cauhoi_id,
        cautraloi_name = p_name
    WHERE cautraloi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_cautraloi(p_id INT)
AS
$$
BEGIN
    DELETE FROM CauTraLoi
    WHERE cautraloi_id = p_id;
END;
$$
LANGUAGE plpgsql;