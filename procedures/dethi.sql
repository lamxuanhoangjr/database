/* Insert */
CREATE OR REPLACE PROCEDURE insert_dethi(
    p_id        INT,
    p_monhoc_id INT,
    p_time      INT,
    p_name      VARCHAR(100)
) AS
$$
BEGIN
    INSERT INTO DeThi (dethi_id, monhoc_id, dethi_time, dethi_name)
    VALUES (p_id, p_monhoc_id, p_time, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_dethi(
    p_id        INT,
    p_monhoc_id INT,
    p_time      INT,
    p_name      VARCHAR(100)
) AS
$$
BEGIN
    UPDATE Dethi
    SET
        monhoc_id = p_monhoc_id,
        dethi_time = p_time,
        dethi_name = p_name
    WHERE dethi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_dethi(p_id VARCHAR(20))
AS
$$
BEGIN
    DELETE FROM DeThi
    WHERE dethi_id = p_id;
END;
$$
LANGUAGE plpgsql;