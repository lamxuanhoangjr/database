/* Insert */
CREATE OR REPLACE PROCEDURE insert_lophoc(
    p_id    INT,
    p_name  VARCHAR(100)
) AS
$$
BEGIN
    INSERT INTO LopHoc (lophoc_id, lophoc_name)
    VALUES (p_id, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_lophoc(
    p_id    INT,
    p_name  VARCHAR(100)
) AS
$$
BEGIN
    UPDATE LopHoc
    SET lophoc_name = p_name
    WHERE lophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_lophoc(p_id INT)
AS
$$
BEGIN
    DELETE FROM LopHoc
    WHERE lophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;