/* Insert */
CREATE OR REPLACE PROCEDURE insert_sinhvien(
    p_id                INT,
    p_nguoidung_account VARCHAR(255),
    p_name              VARCHAR(100),
    p_gender            VARCHAR(3),
    p_birthday          TIMESTAMP WITHOUT TIME ZONE
) AS
$$
BEGIN
    INSERT INTO SinhVien (
        sinhvien_id,
        nguoidung_account,
        sinhvien_name,
        sinhvien_gender,
        sinhvien_birthday
    )
    VALUES (p_id, p_nguoidung_account, p_name, p_gender, p_birthday);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_sinhvien(
    p_id                INT,
    p_nguoidung_account VARCHAR(255),
    p_name              VARCHAR(100),
    p_gender            VARCHAR(3),
    p_birthday          TIMESTAMP WITHOUT TIME ZONE
) AS
$$
BEGIN
    UPDATE SinhVien
    SET
        nguoidung_account = p_nguoidung_account,
        sinhvien_name = p_name,
        sinhvien_gender = p_gender,
        sinhvien_birthday = p_birthday
    WHERE sinhvien_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_sinhvien(p_id INT)
AS
$$
BEGIN
    DELETE FROM SinhVien
    WHERE sinhvien_id = p_id;
END;
$$
LANGUAGE plpgsql;