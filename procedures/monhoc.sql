/* Insert */
CREATE OR REPLACE PROCEDURE insert_monhoc(
    p_id         INT,
    p_lophoc_id  INT,
    p_name       VARCHAR(100)
) AS
$$
BEGIN
    INSERT INTO MonHoc (monhoc_id, lophoc_id, monhoc_name)
    VALUES (p_id, p_lophoc_id, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_monhoc(
    p_id         INT,
    p_lophoc_id  INT,
    p_name       VARCHAR(100)
) AS
$$
BEGIN
    UPDATE MonHoc
    SET
        lophoc_id = p_lophoc_id,
        monhoc_name = p_name
    WHERE monhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_monhoc(p_id INT)
AS
$$
BEGIN
    DELETE FROM MonHoc
    WHERE monhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;