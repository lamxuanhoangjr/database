/* Insert */
CREATE OR REPLACE PROCEDURE insert_baithi(
    p_id           INT,
    p_dethi_id     INT,
    p_sinhvien_id  INT,
    p_startdate    TIMESTAMP WITHOUT TIME ZONE,
    p_enddate      TIMESTAMP WITHOUT TIME ZONE
) AS
$$
BEGIN
    INSERT INTO BaiThi (
        baithi_id,
        dethi_id,
        sinhvien_id,
        baithi_startdate,
        baithi_enddate
    )
    VALUES (p_id, p_dethi_id, p_sinhvien_id, p_gender, p_birthday);
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_baithi(p_id INT)
AS
$$
BEGIN
    DELETE FROM BaiThi
    WHERE baithi_id = p_id;
END;
$$
LANGUAGE plpgsql;
