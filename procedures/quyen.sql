/* Insert */
CREATE OR REPLACE PROCEDURE insert_quyen(
    p_id   INT,
    p_name VARCHAR(100)
) AS
$$
BEGIN
    INSERT INTO Quyen (quyen_id, quyen_name)
    VALUES (p_id, p_name);
END;
$$
LANGUAGE plpgsql;

/* Update */
CREATE OR REPLACE PROCEDURE update_quyen(
    p_id   INT,
    p_name VARCHAR(100)
) AS
$$
BEGIN
    UPDATE Quyen
    SET quyen_name = p_name
    WHERE quyen_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Delete */
CREATE OR REPLACE PROCEDURE delete_quyen(p_id INT)
AS
$$
BEGIN
    DELETE FROM Quyen
    WHERE quyen_id = p_id;
END;
$$
LANGUAGE plpgsql;