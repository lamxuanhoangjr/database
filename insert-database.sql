CREATE DATABASE thi WITH ENCODING 'UTF8';

\c thi;

\i database.sql;

\i functions/baithi.sql;
\i functions/cauhoi.sql;
\i functions/cauhoidethi.sql;
\i functions/cautraloi.sql;
\i functions/cautraloibaithi.sql;
\i functions/chuong.sql;
\i functions/dethi.sql;
\i functions/lophoc.sql;
\i functions/monhoc.sql;
\i functions/nguoidung.sql;
\i functions/quyen.sql;
\i functions/sinhvien.sql;

\i procedures/baithi.sql;
\i procedures/cauhoi.sql;
\i procedures/cauhoidethi.sql;
\i procedures/cautraloi.sql;
\i procedures/cautraloibaithi.sql;
\i procedures/chuong.sql;
\i procedures/dethi.sql;
\i procedures/lophoc.sql;
\i procedures/monhoc.sql;
\i procedures/nguoidung.sql;
\i procedures/quyen.sql;
\i procedures/sinhvien.sql;

\i insert-comments.sql;

\i insert.sql;