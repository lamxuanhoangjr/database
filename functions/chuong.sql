/* Get */
CREATE OR REPLACE FUNCTION get_chuong(p_id INT)
RETURNS TABLE (
    chuong_id     INT,
    monhoc_id     INT,
    chuong_number INT,
    chuong_name   VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT c.chuong_id, c.monhoc_id, c.chuong_number, c.chuong_name
        FROM Chuong c
        WHERE c.chuong_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_chuong_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    chuong_id     INT,
    monhoc_id     INT,
    chuong_number INT,
    chuong_name   VARCHAR(200)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT c.chuong_id, c.monhoc_id, c.chuong_number, c.chuong_name
        FROM Chuong c
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
