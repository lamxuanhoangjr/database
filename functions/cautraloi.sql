/* Get */
CREATE OR REPLACE FUNCTION get_cautraloi(p_id INT)
RETURNS TABLE (
    cautraloi_id   INT,
    cauhoi_id      INT,
    cautraloi_name VARCHAR(800)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ctl.cautraloi_id, ctl.cauhoi_id, ctl.cautraloi_name
        FROM CauTraLoi ctl
        WHERE ctl.cautraloi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_cautraloi_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    cautraloi_id   INT,
    cauhoi_id      INT,
    cautraloi_name VARCHAR(800)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ctl.cautraloi_id, ctl.cauhoi_id, ctl.cautraloi_name
        FROM CauTraLoi ctl
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
