/* Get */
CREATE OR REPLACE FUNCTION get_cauhoi(p_id INT)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  VARCHAR(800)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ch.cauhoi_id, ch.chuong_id, ch.cautraloi_id, mh.cauhoi_name
        FROM CauHoi ch
        WHERE ch.cauhoi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_cauhoi_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    cauhoi_id    INT,
    chuong_id    INT,
    cautraloi_id INT,
    cauhoi_name  VARCHAR(800)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT ch.cauhoi_id, ch.cauhoi_id, ch.cautraloi_id, ch.cauhoi_name
        FROM CauHoi ch
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
