/* Get */
CREATE OR REPLACE FUNCTION get_cauhoidethi(p_id INT)
RETURNS TABLE (
    cauhoidethi_id INT,
    dethi_id       INT,
    cauhoi_id      INT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT chdt.cauhoidethi_id, chdt.dethi_id, chdt.cauhoi_id
        FROM CauHoiDeThi chdt
        WHERE chdt.cauhoidethi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_cauhoidethi_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    cauhoidethi_id INT,
    dethi_id       INT,
    cauhoi_id      INT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT chdt.cauhoidethi_id, chdt.dethi_id, chdt.cauhoi_id
        FROM CauHoiDeThi chdt
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
