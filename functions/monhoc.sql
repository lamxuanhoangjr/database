/* Get */
CREATE OR REPLACE FUNCTION get_monhoc(p_id INT)
RETURNS TABLE (
    monhoc_id  INT,
    lophoc_id  INT,
    monhoc_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT mh.monhoc_id, mh.lophoc_id, mh.monhoc_name
        FROM MonHoc mh
        WHERE mh.monhoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_monhoc_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    monhoc_id   INT,
    lophoc_id   INT,
    monhoc_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT mh.monhoc_id, mh.lophoc_id, mh.monhoc_name
        FROM MonHoc mh
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
