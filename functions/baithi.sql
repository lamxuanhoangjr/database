/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_baithi_withsinhvienid(
    p_id     INT,
    p_limit  INT,
    p_offset INT
)
RETURNS TABLE (
    baithi_id         INT,
    dethi_id          INT,
    sinhvien_id       INT,
    baithi_startdate  TIMESTAMP WITHOUT TIME ZONE,
    baithi_enddate    TIMESTAMP WITHOUT TIME ZONE
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            bt.baithi_id,
            bt.dethi_id,
            bt.sinhvien_id,
            bt.baithi_startdate,
            bt.baithi_enddate
        FROM BaiThi bt
        WHERE bt.sinhvien_id = p_id
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
