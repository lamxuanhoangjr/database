/* Get */
CREATE OR REPLACE FUNCTION get_quyen(p_id INT)
RETURNS TABLE (
    quyen_id   INT,
    quyen_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT q.quyen_id, q.quyen_name
        FROM Quyen q
        WHERE q.quyen_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_quyen_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    quyen_id   INT,
    quyen_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT q.quyen_id, q.quyen_name
        FROM Quyen q
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
