/* Get */
CREATE OR REPLACE FUNCTION get_dethi(p_id VARCHAR(20))
RETURNS TABLE (
    dethi_id   INT,
    monhoc_id  INT,
    dethi_time INT,
    dethi_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT dt.dethi_id, dt.monhoc_id, dt.dethi_time, dt.dethi_name
        FROM DeThi dt
        WHERE dt.dethi_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_dethi_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    dethi_id   INT,
    monhoc_id  INT,
    dethi_time INT,
    dethi_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT dt.dethi_id, dt.monhoc_id, dt.dethi_time, dt.dethi_name
        FROM DeThi dt
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
