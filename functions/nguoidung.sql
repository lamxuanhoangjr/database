/* Get */
CREATE OR REPLACE FUNCTION get_nguoidung(p_account VARCHAR(255))
RETURNS TABLE (
    nguoidung_account     VARCHAR(255),
    quyen_id              INT,
    nguoidung_password    VARCHAR(255),
    nguoidung_displayname VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT nd.nguoidung_account, nd.quyen_id, nd.nguoidung_password, nd.nguoidung_displayname
        FROM NguoiDung nd
        WHERE nd.nguoidung_account = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_nguoidung_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    nguoidung_account     VARCHAR(255),
    quyen_id              INT,
    nguoidung_password    VARCHAR(255),
    nguoidung_displayname VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT nd.nguoidung_account, nd.quyen_id, nd.nguoidung_password, nd.nguoidung_displayname
        FROM NguoiDung nd
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
