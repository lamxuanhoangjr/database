/* Get */
CREATE OR REPLACE FUNCTION get_sinhvien(p_id INT)
RETURNS TABLE (
    sinhvien_id         INT,
    nguoidung_account   VARCHAR(255),
    sinhvien_name       VARCHAR(100),
    sinhvien_gender     VARCHAR(3),
    sinhvien_birthday   TIMESTAMP WITHOUT TIME ZONE
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            sv.sinhvien_id,
            sv.nguoidung_account,
            sv.sinhvien_name,
            sv.sinhvien_gender,
            sv.sinhvien_birthday
        FROM SinhVien sv
        WHERE sv.sinhvien_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_nguoidung_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    nguoidung_account     VARCHAR(255),
    quyen_id              INT,
    nguoidung_password    VARCHAR(255),
    nguoidung_displayname VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            sv.sinhvien_id,
            sv.nguoidung_account,
            sv.sinhvien_name,
            sv.sinhvien_gender,
            sv.sinhvien_birthday
        FROM SinhVien sv
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
