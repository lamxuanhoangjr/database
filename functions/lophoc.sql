/* Get */
CREATE OR REPLACE FUNCTION get_lophoc(p_id INT)
RETURNS TABLE (
    lophoc_id   INT,
    lophoc_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT lh.lophoc_id, lh.lophoc_name
        FROM LopHoc lh
        WHERE lh.lophoc_id = p_id;
END;
$$
LANGUAGE plpgsql;

/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_lophoc_withlimitoffset(
  p_limit INT,
  p_offset INT
)
RETURNS TABLE (
    lophoc_id   INT,
    lophoc_name VARCHAR(100)
) AS
$$
BEGIN
    RETURN QUERY
        SELECT lh.lophoc_id, lh.lophoc_name
        FROM LopHoc lh
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
