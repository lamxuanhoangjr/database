/* Get All with limit and offset */
CREATE OR REPLACE FUNCTION getall_cautraloibaithi(
    p_id     INT
)
RETURNS TABLE (
    cautraloibaithi_id INT,
    baithi_id          INT,
    cautraloi_id       INT
) AS
$$
BEGIN
    RETURN QUERY
        SELECT
            ctlbt.cautraloibaithi_id,
            ctlbt.baithi_id,
            ctlbt.cautraloi_id
        FROM CauTraLoiBaiThi ctlbt
        WHERE ctlbt.baithi_id = p_id
        LIMIT p_limit
        OFFSET p_offset;
END;
$$
LANGUAGE plpgsql;
