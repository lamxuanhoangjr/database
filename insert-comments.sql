-- Comment on functions
COMMENT ON FUNCTION getall_baithi_withsinhvienid(
    p_id     INT,
    p_limit  INT,
    p_offset INT
) IS 'Lấy các bảng BaiThi từ sinhvien_id với giới hạn limit và offset';

COMMENT ON FUNCTION getall_cautraloibaithi(p_id INT)
IS 'Lấy các bảng CauTraLoi từ baithi_id';

-- Comment on procedures